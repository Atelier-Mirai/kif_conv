require "kif_conv/version"

module KifConv
  # kifuがどの形式かを判定し、format形式へ変換する
  def self.convert(kifu, format)
  end

  # 棋譜形式を判定する
  def self.check_format(kifu)
    # 棋譜中のコメントを削除した文字列を作る(形式判定のため)
    lines = []
    kifu.split("\r\n").each do |k|
      if k.exclude?("*") && k.exclude?("'")
        lines << k
      end
    end

    regex = ("[A-Za-z0-9]+\/" * 9).chop
    lines.each do |line|
      if tmp.include?("手数-") && tmp.include?("指手-")
        return :kif
      end
      if line.include?("+---------------------------+")
        return :bod
      end
      if (tmp.include?("V") && tmp.include?("P")) &&
         (tmp.include?("+") && tmp.include?("-"))
        return :csa
      end
      if line.match(regex)
        # sfen形式
        # lnsgkgsn1/1r5b1/ppppppppp/9/9/9/9/9/LNSGKGSNL
        return :sfen
      end
    end
    return :no_kifu
  end
end
